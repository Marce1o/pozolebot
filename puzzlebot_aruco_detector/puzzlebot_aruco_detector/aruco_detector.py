#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image, CameraInfo
from geometry_msgs.msg import PoseStamped
from vision_msgs.msg import Detection2DArray, Detection2D, ObjectHypothesisWithPose
from cv_bridge import CvBridge
import cv2.aruco as aruco
import cv2
import numpy as np

class ArucoMarkerDetector(Node):
    def __init__(self):
        super().__init__('aruco_marker_detector')
        self.subscription = self.create_subscription(Image, 'camera/image', self.image_callback, 10)
        self.publisher = self.create_publisher(PoseStamped, 'aruco_detections', 10)
        self.br = CvBridge()
        self.aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_1000)
        self.get_logger().info('1000')
        self.aruco_params = cv2.aruco.DetectorParameters_create()

        # parametros para la camara
        self.camera_matrix = np.array([[473.745540, 0, 324.806398], [0, 473.721603, 238.711882], [0, 0, 1]], dtype=float)  # Tenemos que sacar la matriz de la camara
        self.dist_coeffs = np.array([0.018566, -0.050238, -0.001339, 0.002664, 0.000000]) #checar despues dependiendo de la distorcion de la camara

    def image_callback(self, msg):
        # self.get_logger().info('Received image')
        cv_image = self.br.imgmsg_to_cv2(msg, 'bgr8')
        gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        corners, ids, _ = cv2.aruco.detectMarkers(gray, self.aruco_dict, parameters=self.aruco_params)

        if ids is not None:
            self.get_logger().info('Aruco Detected')
            detections_msg = PoseStamped()
            for i in range(len(ids)):
                rvec, tvec, _ = cv2.aruco.estimatePoseSingleMarkers(corners[i], 0.05, self.camera_matrix, self.dist_coeffs)
                detections_msg.header.stamp = self.get_clock().now().to_msg()
                detections_msg.header.frame_id = 'camera_frame'
                detections_msg.pose.position.x = tvec[0][0][0]
                detections_msg.pose.position.y = tvec[0][0][1]
                detections_msg.pose.position.z = tvec[0][0][2]
                detections_msg.pose.orientation.x = rvec[0][0][0]
                detections_msg.pose.orientation.y = rvec[0][0][1]
                detections_msg.pose.orientation.z = rvec[0][0][2]
                detections_msg.pose.orientation.w = 1.0

                self.publisher.publish(detections_msg)
                self.get_logger().info('Published pose from aruco {}'.format(i))

                aruco.drawDetectedMarkers(cv_image,corners,ids)
                aruco.drawAxis(cv_image,self.camera_matrix, self.dist_coeffs, rvec, tvec, 0.1)
        cv2.imshow('Aruco Pose Estimation', cv_image)
        cv2.waitKey(1)

def main(args=None):
    rclpy.init(args=args)
    node = ArucoMarkerDetector()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
