#!/usr/bin/env python3
from math import sin, cos, pi

import rclpy
import time
from rclpy.node import Node
import numpy as np
from std_msgs.msg import Float32
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from tf2_msgs.msg import TFMessage
from tf2_ros import TransformBroadcaster, TransformStamped

x=0.0
y=0.0
theta= Quaternion()
w=0.0
v=0.0
wl=0.0
wr=0.0

#callback
def callback_odometry(msg):
    global x,y,theta, w, v
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    theta = msg.pose.pose.orientation
    w = msg.twist.twist.angular.z
    v = msg.twist.twist.linear.x
  
def callback_wl(msg):
    global wl 
    wl = msg.data

def callback_wr(msg):
    global wr 
    wr = msg.data

def euler_to_quaternion(roll, pitch, yaw):
    qx = sin(roll/2) * cos(pitch/2) * cos(yaw/2) - cos(roll/2) * sin(pitch/2) * sin(yaw/2)
    qy = cos(roll/2) * sin(pitch/2) * cos(yaw/2) + sin(roll/2) * cos(pitch/2) * sin(yaw/2)
    qz = cos(roll/2) * cos(pitch/2) * sin(yaw/2) - sin(roll/2) * sin(pitch/2) * cos(yaw/2)
    qw = cos(roll/2) * cos(pitch/2) * cos(yaw/2) + sin(roll/2) * sin(pitch/2) * sin(yaw/2)
    return Quaternion(x=qx, y=qy, z=qz, w=qw)

def main():
    rclpy.init()
    node = Node('joint_State_publisher')


    #publishers and subscriber
    odometry_subscriber = node.create_subscription(Odometry, '/odom', callback_odometry, 10)
    wr_subscriber = node.create_subscription(Float32, '/wr', callback_wr, 10)
    wl_subscriber = node.create_subscription(Float32, '/wl', callback_wl, 10)
    
    tf_publisher = node.create_publisher(TFMessage, '/tf', 10)
    joint_State_publisher = node.create_publisher(JointState, '/joint_states', 10)
    broadcaster_odom = TransformBroadcaster(node)
    broadcaster_wr = TransformBroadcaster(node)
    broadcaster_wl = TransformBroadcaster(node)
    odom_trans = TransformStamped()
    wr_trans = TransformStamped()
    wl_trans = TransformStamped()
    joint_state = JointState()

    odom_trans.header.frame_id = 'odom'
    odom_trans.child_frame_id = 'base_link'
    wr_trans.header.frame_id = 'base_link'
    wr_trans.child_frame_id = 'wr_link'
    wl_trans.header.frame_id = 'base_link'
    wl_trans.child_frame_id = 'wl_link'
    joint_state = JointState()

    #robot state
    
    t_l=0.0
    t_r=0.0
    start_timer = node.get_clock().now()
    #message
    msg = Odometry()
    position = Point()
    rate = node.create_rate(10)
    print("The Joint State Publisher is running")

    try:
        while rclpy.ok():
            current_time = node.get_clock().now()
            duration = current_time - start_timer
            dt = duration.nanoseconds * 1e-9

            # update joint_state
            now = node.get_clock().now()
            joint_state.header.stamp = now.to_msg()
        
            
            odom_trans.header.stamp =now.to_msg()
            odom_trans.transform.translation.x = x
            odom_trans.transform.translation.y = y
            odom_trans.transform.translation.z = 0.0
            odom_trans.transform.rotation = theta

           # joint_State_publisher.publish(joint_state)           
            broadcaster_odom.sendTransform(odom_trans)

            t_r += wr*dt
            wr_trans.header.stamp =now.to_msg()
            wr_trans.transform.translation.x = 0.0
            wr_trans.transform.translation.y = 0.0
            wr_trans.transform.translation.z = 0.0
            wr_trans.transform.rotation = \
                    euler_to_quaternion(0., t_r, 0.)

           # joint_State_publisher.publish(joint_state)           
            broadcaster_wr.sendTransform(wr_trans)

            
            t_l += wl*dt
            wl_trans.header.stamp =now.to_msg()
            wl_trans.transform.translation.x = 0.0
            wl_trans.transform.translation.y = 0.0
            wl_trans.transform.translation.z = 0.0
            wl_trans.transform.rotation = \
                    euler_to_quaternion(0., t_l, 0.0)
 
           # joint_State_publisher.publish(joint_state)           
            broadcaster_wl.sendTransform(wl_trans)

            
            start_timer = current_time

            #Create new robot state
            rclpy.spin_once(node)
            
    except KeyboardInterrupt:
        pass

    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__=='__main__':
    main()