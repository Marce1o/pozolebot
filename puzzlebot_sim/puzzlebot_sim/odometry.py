#!/usr/bin/env python3
# odom.py

import rclpy
import time
from rclpy.node import Node
import numpy as np
from std_msgs.msg import Float32
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion

#Puzzlebot Parameters global
r = 0.05
b = 0.19
v = 0
w = 0
wr = 0.0
wl = 0.0

max_w = 1.25
max_l = 0.6


#callback
def callback_wr(msg):
    global wr
    wr = msg.data

def callback_wl(msg):
    global wl 
    wl = msg.data

def update_vel():
    global wr, wl, v, w
    v = r * (wr + wl) / 2
    w = r * (wr - wl) / (2*b)

def update_pos(x, y, theta, dt):
    x = x + v*np.cos(theta) *dt
    y = y + v*np.sin(theta) *dt
    print(x,y)
    theta = theta + w*dt
    return x, y, theta    

def update_covariance(theta, Qk, covMat, v, dt):
    H = np.array(
        [[1.0, 0.0, -dt*v*np.sin(theta)],
        [0.0, 1.0, dt*v*np.cos(theta)],
        [0.0, 0.0, 1.0]]
        )
    # print(np.ndarray.transpose(H))
    temp = covMat
    covMat = H @ temp @ np.ndarray.transpose(H) + Qk
    # print(covMat)
    return covMat

def update_error_mat(wl, wr, kl, kr, theta, r, l, Qk, dt):
    noise = np.array(
        [[kr*np.abs(wr), 0],
        [0, kl*np.abs(wl)]
        ]
        )
    Vw = np.array(
        [[np.cos(theta), np.cos(theta)],
         [np.sin(theta), np.sin(theta)],
         [2/l, -2/l]
        ]
        )
    # print(Vw)
    Vw = Vw * dt * r * 0.5
    Qk = Vw @ noise @ np.ndarray.transpose(Vw)
    # print(Vw)
    return Qk

def quaternion_from_euler(roll, pitch, yaw):
    """
    Converts euler roll, pitch, yaw to quaternion (w in last place)
    quat = [x, y, z, w]
    Bellow should be replaced when porting for ROS 2 Python tf_conversions is done.
    """
    cy = np.cos(yaw * 0.5)
    sy = np.sin(yaw * 0.5)
    cp = np.cos(pitch * 0.5)
    sp = np.sin(pitch * 0.5)
    cr = np.cos(roll * 0.5)
    sr = np.sin(roll * 0.5)


    w_quat = cy * cp * cr + sy * sp * sr
    x_quat = cy * cp * sr - sy * sp * cr
    y_quat = sy * cp * sr + cy * sp * cr
    z_quat = sy * cp * cr - cy * sp * sr

    return w_quat, x_quat, y_quat, z_quat

def wrap_to_Pi(theta):
    result = np.fmod((theta),(2 * np.pi))
    if(result < -1*np.pi):
        result += 2 * np.pi
    return result


def main():
    rclpy.init()
    node = Node('odometry')

    x = 0
    y = 0
    theta = 0
    kr = 0.01
    kl = 0.01
    covMsg = [0.0]*36
    
    Qk = np.array(
        [[0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0]]
        )
    covMat = np.array(
        [[0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0]]
        )

    start_timer = node.get_clock().now()

    #publishers and subscriber
    odometry_publisher = node.create_publisher(Odometry, '/odom', 10)
    wl_subscriber = node.create_subscription(Float32, '/wl',callback_wl, 1)
    wr_subscriber = node.create_subscription(Float32, '/wr', callback_wr, 1)

    #message
    msg = Odometry()
    position = Point()
    rate = node.create_rate(10)
    print("The Cinematic Model is running")

    try:
        while rclpy.ok():
            current_time = node.get_clock().now()
            duration = current_time - start_timer
            dt = duration.nanoseconds * 1e-9

            update_vel()

            x, y, theta = update_pos(x, y, theta, dt)
            theta = wrap_to_Pi(theta)
            Qk = update_error_mat(wl, wr, kl, kr, theta, r, b, Qk, dt)

            covMat = update_covariance(theta, Qk, covMat, v, dt)
            covMsg[0], covMsg[1], covMsg[5] = covMat[0,:]
            covMsg[6], covMsg[7], covMsg[11] = covMat[1,:]
            covMsg[30], covMsg[31], covMsg[35] = covMat[2,:]

            print(covMsg)

            msg.header.stamp = current_time.to_msg()
            
            # Position
            position.x = x
            position.y = y
            position.z = 0.0
            
            # Orientation
            (w_quat,x_quat,y_quat,z_quat) = quaternion_from_euler(0.0,0.0,theta)
            quat = Quaternion(x=x_quat, y=y_quat, z=z_quat, w=w_quat)

            msg.header.frame_id = 'odom'
            msg.child_frame_id = 'base_link'
            msg.twist.twist.linear.x = v
            msg.twist.twist.angular.z = w
            msg.pose.pose.orientation= quat
            msg.pose.pose.position = position
            msg.pose.covariance = covMsg
            odometry_publisher.publish(msg)

            start_timer = current_time

            rclpy.spin_once(node)

    except KeyboardInterrupt:
        pass

    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__=='__main__':
    main()
