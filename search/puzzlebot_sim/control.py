#!/usr/bin/env python3

import math
import rclpy
import time
from rclpy.node import Node
import numpy as np
from std_msgs.msg import Float32
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from nav_msgs.msg import Odometry

import math

#Puzzlebot Parameters global
r = 0.05
b = 0.19
v = 0.
w = 0.
x = 0.0
y = 0.0
x_setpoint = 0.0
y_setpoint = 0.0
theta = 0.0

xdot= 0.
ydot= 0.
tethdot= 0.

linearX = 0.
control_output_teth = 0.
error_integral_l = 0.
error_integral_t = 0.
error_prev_l = 0.  
error_prev_t = 0.

max_w = 1.25
max_l = 0.9
#callback

# Constantes del controlador PID
kp = 0.4 # Ganancia proporcional
ki = 0.0  # Ganancia integral
kd = 0.05 # Ganancia derivativa

kpw = 1.4
kiw = 0.0
kdw = 1.0

# Variables para el término integral y derivativo

def truncated_remainder(dividend, divisor):
    divided_number = dividend / divisor
    divided_number = \
        -int(-divided_number) if divided_number < 0 else int(divided_number)

    remainder = dividend - divisor * divided_number

    return remainder

def wrap_to_pi(input_angle):
    p1 = truncated_remainder(input_angle + np.sign(input_angle) * math.pi, 2 * math.pi)
    p2 = (np.sign(np.sign(input_angle)
                  + 2 * (np.sign(abs((truncated_remainder(input_angle + math.pi, 2 * math.pi))
                                      / (2 * math.pi))) - 1))) * math.pi

    output_angle = p1 - p2

    return output_angle
    
def euler_from_quaternion(quaternion):
    """
    Converts quaternion (w in last place) to euler roll, pitch, yaw
    quaternion = [x, y, z, w]
    Bellow should be replaced when porting for ROS 2 Python tf_conversions is done.
    """
    x = quaternion.x
    y = quaternion.y
    z = quaternion.z
    w = quaternion.w

    sinr_cosp = 2 * (w * x + y * z)
    cosr_cosp = 1 - 2 * (x * x + y * y)
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    sinp = 2 * (w * y - z * x)
    pitch = np.arcsin(sinp)

    siny_cosp = 2 * (w * z + x * y)
    cosy_cosp = 1 - 2 * (y * y + z * z)
    yaw = np.arctan2(siny_cosp, cosy_cosp)

    return roll, pitch, yaw

# Función de control PID
def control(dt): 
    global x, y, x_setpoint, y_setpoint, theta, control_output_teth, linearX, error_integral_l,error_integral_t, error_prev_l, error_prev_t
    
    # Calcula el error
    error_x = x_setpoint - x
    error_y = y_setpoint - y
    error_l = math.sqrt(error_x ** 2 + error_y ** 2)
    
    teth_setpoint = math.atan2(error_y,error_x) 
    error_teth = wrap_to_pi(teth_setpoint - theta) 

    # Término proporcional
    p_term_l = kp * error_l
    p_term_teth = kpw * error_teth

    # Término integral
    error_integral_l += error_l*dt
    i_term_l = ki * error_integral_l

    error_integral_t += error_teth*dt
    i_term_teth = kiw * error_integral_t

    # Término derivativo
    d_term_l = kd * (error_l - error_prev_l)/dt
    d_term_teth = kdw * (error_teth - error_prev_t)/dt

    error_prev_l = error_l
    error_prev_t = error_teth

    

    # Calcula la salida del controlador PID
 
    control_output_teth = p_term_teth + i_term_teth + d_term_teth

   
    # Pit
    linearX = p_term_l + d_term_l + i_term_l

     
    if (error_teth>= -0.05 and error_teth <= 0.05):
        control_output_teth = 0.0

    if (error_l >= -0.05 and error_l <= 0.05):
        linearX = 0.0
    
    if (control_output_teth > max_w):
        control_output_teth = max_w
    
    if (linearX > max_l):
        linearX = max_l

    return teth_setpoint

def callback_odom(msg):
    global x,y,theta
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    roll, pitch, yaw = euler_from_quaternion(msg.pose.pose.orientation)
    theta = yaw

def callback_setpoint(msg):
    global x_setpoint,y_setpoint
    x_setpoint = msg.pose.position.x

    y_setpoint = msg.pose.position.y


def main():
    rclpy.init()
    node = Node('simulation')

    #publishers and subscriber
    cmdVel_publisher = node.create_publisher(Twist, '/cmd_vel', 10)
    goal_theta_publisher = node.create_publisher(Float32, '/theta_goal', 10)
    odom_subscriber = node.create_subscription(Odometry, '/odom', callback_odom, 10)
    setpoint_subscriber = node.create_subscription(PoseStamped, '/goal_pose', callback_setpoint, 10)
    #message
    vels = Twist()
    msg = Float32()
    position = Point()
    rate = node.create_rate(100)
    print("The Controller is running")
    start_timer = node.get_clock().now()

    try:
        while rclpy.ok():
            current_time = node.get_clock().now()
            duration = current_time - start_timer
            dt = duration.nanoseconds * 1e-9
            msg.data = control(dt)
            
            vels.linear.x = linearX
            vels.angular.z = control_output_teth
            cmdVel_publisher.publish(vels)
            goal_theta_publisher.publish(msg)
            start_timer = current_time

            rclpy.spin_once(node)

    except KeyboardInterrupt:
        pass

    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__=='__main__':
    main()
