#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
import numpy as np
from std_msgs.msg import Float32
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion

#Puzzlebot Parameters global
r = 0.05
b = 0.19
v = 0
w = 0

#callback
def callback_cmd_vel(msg):
    global v, w 
    v = msg.linear.x
    w = msg.angular.z
    
def update_pos(x,y,theta, dt):
    global v, w
    x = x + v*np.cos(theta) *dt
    y = y + v*np.sin(theta) *dt
    theta = theta + w*dt
    return x, y, theta

def wheels(wr, wl):
    wr.data = v/r + (w*b)/r
    wl.data = v/r - (w*b)/r

def wrap_to_Pi(theta):
    result = np.fmod((theta),(2 * np.pi))
    if(result < -1*np.pi):
        result += 2 * np.pi
    return result


def quaternion_from_euler(roll, pitch, yaw):
    """
    Converts euler roll, pitch, yaw to quaternion (w in last place)
    quat = [x, y, z, w]
    Bellow should be replaced when porting for ROS 2 Python tf_conversions is done.
    """
    cy = np.cos(yaw * 0.5)
    sy = np.sin(yaw * 0.5)
    cp = np.cos(pitch * 0.5)
    sp = np.sin(pitch * 0.5)
    cr = np.cos(roll * 0.5)
    sr = np.sin(roll * 0.5)

    
    w_quat = cy * cp * cr + sy * sp * sr
    x_quat = cy * cp * sr - sy * sp * cr
    y_quat = sy * cp * sr + cy * sp * cr
    z_quat = sy * cp * cr - cy * sp * sr

    return w_quat, x_quat, y_quat, z_quat


def main():
    rclpy.init()
    node = Node('simulation')

    wr = Float32()
    wl = Float32()

    x = 0.0
    y = 0.0
    theta = 0.0

    start_timer = node.get_clock().now()
    #publishers and subscriber
    Pose_publisher = node.create_publisher(PoseStamped, '/pose', 10)
    wl_publisher = node.create_publisher(Float32, '/wl', 1)
    wr_publisher = node.create_publisher(Float32, '/wr', 1)
    cmdVel_subscriber = node.create_subscription(Twist, '/cmd_vel', callback_cmd_vel, 10)

    #message
    msg = PoseStamped()
    position = Point()
    rate = node.create_rate(100)
    print("The Cinematic Model is running")

    try:
        while rclpy.ok():
            
            current_time = node.get_clock().now()
            duration = current_time - start_timer
            dt = duration.nanoseconds * 1e-9

            x, y, theta = update_pos(x,y,theta,dt)
            theta = wrap_to_Pi(theta)
            msg.header.stamp = node.get_clock().now().to_msg()
            
            position.x = x
            position.y = y

            (w_quat,x_quat,y_quat,z_quat) = quaternion_from_euler(0.0,0.0,theta)

            quat = Quaternion(x=x_quat, y=y_quat, z=z_quat, w=w_quat)
            
            msg.pose.orientation= quat
            msg.pose.position = position
            Pose_publisher.publish(msg)
            wheels(wr, wl)

            wr_publisher.publish(wr)
            wl_publisher.publish(wl)
            
            start_timer = current_time

            rclpy.spin_once(node)

    except KeyboardInterrupt:
        pass

    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__=='__main__':
    main()
