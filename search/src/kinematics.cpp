#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include <tf2/LinearMath/Quaternion.h>


// radius 5cm
// wheelbase 19cm
// input Twist msg -> /cmd_vel
// output PoseStamped msg -> /pose
// output Float32 msg -> /wr /wl wheel speed

class KinematicModel : public rclcpp::Node
{
public:
	// Constructor
    KinematicModel()
    : Node("kinematic_model")
	{
	    // Initialize Publishers and subscribers
	    pose_pub_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("pose",10);
	    wr_pub_ = this->create_publisher<std_msgs::msg::Float32>("wr",10);
	    wl_pub_ = this->create_publisher<std_msgs::msg::Float32>("wl",10);

	    cmd_vel_sub_ = this->create_subscription<geometry_msgs::msg::Twist>("cmd_vel", 10,
			    std::bind(&KinematicModel::cmd_vel_cb, this, std::placeholders::_1));
	    RCLCPP_INFO(this->get_logger(), "Kinematic Model node has started.");
            
	    timer_ = this->create_wall_timer(std::chrono::milliseconds(10),
			    std::bind(&KinematicModel::update, this));

	}
protected:
    // cmd_vel topic callback
    void cmd_vel_cb(const geometry_msgs::msg::Twist & msg){
    	//RCLCPP_INFO(this->get_logger(), "v:%f", msg.linear.x);
    	//RCLCPP_INFO(this->get_logger(), "w:%f", msg.angular.z);
	cmd_vel_ = msg;
    }

   // update robot pose and wheel velocities
   void update(){
	// Get Linear and Angular Velocities
	double u = cmd_vel_.linear.x;
	double omega = cmd_vel_.angular.z;
	
	// Update pose
	yaw_ = yaw_ + omega*dt;

	currentPose_.pose.position.x += u * std::cos(yaw_) * dt;
	currentPose_.pose.position.y += u * std::sin(yaw_) * dt;

	
        //RCLCPP_INFO_STREAM(rclcpp::get_logger("rclcpp"), "x = " << currentPose_.pose.position.x << ", y = " << currentPose_.pose.position.y);
	//RCLCPP_INFO_STREAM(rclcpp::get_logger("rclcpp"), "Yaw = " << yaw_ << ", omega = " << omega);

	// publish pose
	currentPose_.header.stamp = this->get_clock()->now();
	currentPose_.header.frame_id = "base_link";

	// Quaternions for orientation
	tf2::Quaternion q;
	q.setRPY(0, 0, yaw_);
	currentPose_.pose.orientation.x = q.x();
	currentPose_.pose.orientation.y = q.y();
	currentPose_.pose.orientation.z = q.z();
	currentPose_.pose.orientation.w = q.w();

	pose_pub_->publish(currentPose_);
	
        // Publish wheel velocities from cmd_vel	
	std_msgs::msg::Float32 leftMsg, rightMsg;
	rightMsg.data = (u / r) + ((b*omega) / r); 
	leftMsg.data = (u / r) - ((b*omega) / r); 

	wr_pub_->publish(rightMsg);
	wl_pub_->publish(leftMsg);

   }
private:
    // Publishers and Subscribers
    rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr pose_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr wr_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr wl_pub_;

    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr cmd_vel_sub_;

    rclcpp::TimerBase::SharedPtr timer_;


    
    // Variables
    geometry_msgs::msg::Twist cmd_vel_;
    double yaw_{0};
    geometry_msgs::msg::PoseStamped currentPose_;

    //Simulation Constants
    double dt{0.01};
    double r{0.05}; // wheel Radius
    double b{0.19}; // robot base
};

int main(int argc, char **argv)
{
    // Init Node
    rclcpp::init(argc,argv);
    auto node = std::make_shared<KinematicModel>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
