from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='servoGripper',
            executable='gripper',
            name='gripper',
            output='screen'
        ),
        Node(
            package='servoGripper',
            executable='object_detection',
            name='object_detection',
            output='screen',
            parameters=[
                {'goal_marker': 1}
            ]
        )
    ]) 
