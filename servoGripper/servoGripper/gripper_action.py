import rclpy
from rclpy.node import Node

from std_msgs.msg import Int32, Bool


class GripperAction(Node):
 
    def __init__(self):
        super().__init__('gripper_action')
        self.gripper_close = False
        # Recieve gripper order
        self.subscription = self.create_subscription(
            Bool,
            '/gripper',
            self.gripper_callback,
            10)
        # self.subscription
        self.publisher_ = self.create_publisher(Int32, '/ServoAngle', 10)
        self.send_angle()

    # TODO: Change msg format
    def gripper_callback(self, msg):
        self.gripper_close = msg.data
        # self.state = msg.data
        self.send_angle()
        # print(self.state)

    def send_angle(self):
        # print("Send angle")
        msg = Int32()
        if (self.gripper_close):
            print("Closing gripper")
            # Define ángulo de cierre de gripper
            angle = 60
        else:
            print("Opening gripper")
            angle = -60
        # print(angle)
        msg.data = angle
        self.publisher_.publish(msg)


def main(args=None):
    rclpy.init(args=args)

    gripper_action = GripperAction()

    # gripper_action.send_angle()

    rclpy.spin(gripper_action)

    # Destroy the node explicitly
    gripper_action.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()