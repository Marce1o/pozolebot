import rclpy
from rclpy.node import Node
from rclpy.parameter import Parameter
import numpy as np
from std_msgs.msg import Int32, Bool
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry


class ObjectDetecion(Node):
    def __init__(self):
        super().__init__('gripper_action')
        self.declare_parameter('goal_marker', 0)
        self.goal_marker = self.get_parameter('goal_marker').get_parameter_value().integer_value
        self.object_marker = 6

        self.follow_object = False
        self.grab_object = False
        self.follow_goal = False

        self.distance_from_object = 0.20
        self.distance_from_goal = 0.20

        self.robot_x = 0.0
        self.robot_y = 0.0
        self.robot_yaw = 0.0

        self.aruco_goal = Point()
        self.aruco_goal.x = 0.0 #msg.pose.x
        self.aruco_goal.y = 0.0 #msg.pose.y

        # Recieve arucos TODO: Use correct message
        self.subscription = self.create_subscription(
            Int32,
            '/arucos',
            self.arucos_callback,
            10)
        # self.pose_sub = self.create_subscription(
        #     Odometry,
        #     '/odom',
        #     self.pose_callback,
        #     10)
        self.test_pose_sub = self.create_subscription(
            Point,
            '/test_odom',
            self.test_pose_callback,
            10)
        self.setpoint_pub = self.create_publisher(Point, '/set', 10)
        self.gripper_pub = self.create_publisher(Bool, '/gripper', 10)

        # Create a timer that fires every 0.1 seconds (adjust as needed)
        self.timer = self.create_timer(0.1, self.timer_callback)
        print("object detecion started")

    def timer_callback(self):
        if self.follow_goal or self.follow_object:
            self.check_aruco_distance()

    # TODO: Use correct message
    def arucos_callback(self, msg):
        # TEST VARIABLES ----
        aruco_id = msg.data #msg.id
        #Pose referenced from bot frame
        # TODO: Change Point to pose
        self.aruco_goal.x = 1.0 #msg.pose.x
        self.aruco_goal.y = 1.0 #msg.pose.y
        # ---------------------

        self.search_aruco(aruco_id)
    
    # TEST CALLBACK ------
    def test_pose_callback(self, msg):
        self.robot_x = msg.x
        self.robot_y = msg.y
        self.robot_yaw = msg.z

    # --------------------

    def pose_callback(self, msg):
        self.robot_x = msg.pose.pose.position.x
        self.robot_y = msg.pose.pose.position.y
        # self.robot_yaw = msg.pose.pose.position.z

    def search_aruco(self, aruco_id):
        if(not self.follow_object and not self.follow_goal):
            if(self.grab_object):
                if(aruco_id == self.goal_marker):
                    self.follow_goal = True
                    print("Follow goal")  
            elif(aruco_id == self.object_marker):
                self.follow_object = True
                print("Follow object")

        elif(self.follow_object and aruco_id == self.object_marker):
            # self.aruco_goal.x = aruco_pose_x - self.distance_from_object
            # self.aruco_goal.y = aruco_pose_y
            self.setpoint_pub.publish(aruco_goal)

        elif(self.follow_goal and aruco_id == self.goal_marker):
            # self.aruco_goal.x = aruco_pose_x - self.distance_from_goal
            # self.aruco_goal.y = aruco_pose_y
            self.setpoint_pub.publish(aruco_goal)

            
    # General logic
    def check_aruco_distance(self):
        close_gripper = Bool()
        close_gripper.data = False
        dist_thresh = 0.01
        angle_thresh = 0.01
        error_x = self.aruco_goal.x - self.robot_x
        error_y = self.aruco_goal.y - self.robot_y
        error_l = np.sqrt(error_x ** 2 + error_y ** 2)

        error_yaw = 0.0
        
        if(error_l <= dist_thresh and error_yaw <= angle_thresh):
            if(self.follow_object):
                self.follow_object = False
                close_gripper.data = True
                self.gripper_pub.publish(close_gripper)
                # TODO: Check if grabbed
                self.grab_object = True
                print("Grab object")
            elif(self.follow_goal):
                self.follow_goal = False
                self.gripper_pub.publish(close_gripper)
                self.grab_object = False
                print("Release object")



def main(args=None):
    rclpy.init(args=args)

    object_detection = ObjectDetecion()

    rclpy.spin(object_detection)

    # Destroy the node explicitly
    object_detection.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()